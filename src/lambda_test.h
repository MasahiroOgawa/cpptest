#ifndef LAMBDA_TEST_H
#define LAMBDA_TEST_H


class Lambda
{
public:
    Lambda();
    bool capture_test(double y);
};

#endif // LAMBDA_TEST_H
