#include <iostream>
#include "date_test.h"
#include "lambda_test.h"
#include "literal_test.h"
#include "stod_test.h"
#include "thread_test.h"

using namespace std;

int main(int argc, char** argv)
try{
    Lambda l;
    l.capture_test(3.0);

    Stod_test stod_test;
    stod_test.except();

    Date_test date_test;
    date_test.run();

    Thread_test thread_test;
    thread_test.run();

    Literal_test literal_test;
    literal_test.run();

    cout << "---\nexit test\n";
}catch(runtime_error& e){
    cerr << "runtime_error: " << e.what() << endl;
    return 1;
}catch(logic_error& e){
    cerr << "logic error: " << e.what() << endl;
    return 1;
}catch(...){
    cerr << "unknown erro!\n";
    return 2;
}
