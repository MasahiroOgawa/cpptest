#include "stod_test.h"
#include <string> //for stod
#include <stdexcept> //for invalid_argument
#include <iostream> //for cerr
using namespace std;

Stod_test::Stod_test()
{
    cout << "---\n"
         << "enter " << __func__ << endl;
}


bool Stod_test::except()try{
    double d = stod("--");
    return true;
}catch(invalid_argument& e){
    cerr << "invalid argument @" << __FILE__ << ", line: " << __LINE__
         << ", message: " << e.what() << endl;
    return false;
    // don't rethrow because this is intended exception.
}catch(...){
throw;
}
