#ifndef THREAD_TEST_H
#define THREAD_TEST_H


class Thread_test
{
public:
    Thread_test();
    void run();
private:
    void vector_thread_test();
    void say_hello();
};

#endif // THREAD_TEST_H
