#ifndef DATE_TEST_H
#define DATE_TEST_H
#include <ctime>

class Date_test
{
public:
    Date_test();
    void run();
private:
    void compare_date();

    tm current_date_;
    tm past_date_;
};

#endif // DATE_TEST_H
