#include "date_test.h"
#include <iostream>
using namespace std;

Date_test::Date_test()
{
    cout << "---\n"
         << "enter " << __func__ << endl;
}

//---------------------------
void Date_test::run(){
    compare_date();
}

//---------------------------

void Date_test::compare_date()try{
    // current date
    time_t current = time(0);
    current_date_ = *localtime(&current);
    cout << "The current local date and time is: " << asctime(&current_date_) << endl;

    //create past_date
    past_date_.tm_sec = 10;
    past_date_.tm_min = 5;
    past_date_.tm_hour = 12;
    past_date_.tm_mday = 2;
    past_date_.tm_mon = 8; //[0,11]
    past_date_.tm_year = 78; //since 1900
    cout << "past date is : " << asctime(&past_date_) << endl;
    time_t past = mktime(&past_date_);

    //compare
    double diff = difftime(past, current);
    cout << "past - current = " << diff << endl;

}catch(runtime_error& e){
    cerr << "@" << __FILE__ << " line: " << __LINE__ << e.what() << endl;
    throw;
}catch(...){
cerr << __FILE__ << "unknown error!";
throw;
}
