#include "thread_test.h"
#include <vector>
#include <thread>
#include <iostream>
#include <algorithm> // for for_each

using namespace std;

Thread_test::Thread_test()
{
    cout << "---\n"
         << "enter " << __func__ << endl;
}

//---

void Thread_test::run(){
    vector_thread_test();
}

//---

void Thread_test::vector_thread_test(){
    vector<thread> v;
    v.push_back(thread(&Thread_test::say_hello, this));
    v.push_back(thread(&Thread_test::say_hello, this));
    for_each(v.begin(), v.end(), [](thread& t){t.join();});
}

//---

void Thread_test::say_hello(){
    cout << "hello\n";
}
