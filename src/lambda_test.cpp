#include "lambda_test.h"
#include <iostream>
using namespace std;

Lambda::Lambda()
{
    cout << "---\n"
         << "enter " << __func__ << endl;
}


bool Lambda::capture_test(double y){
    //from Bjarne
    auto z1 = [=](int x){ return x+y; };
    auto z2 = [=,&y]{if(y) return 1; else return 2; };

    return true;
}

